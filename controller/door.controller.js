const { Door, Sensor } = require('../models');

exports.getDoors = async (req, res) => {
    res.send({ item: await Sensor.findAll({ include: [Door] }) });
};