'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */

    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn('users', 'last_name', {
          type: Sequelize.DataTypes.STRING,
          after: 'first_name',
          allowNull: false,
        }, { transaction: t }),
        queryInterface.addColumn('users', 'email', {
          type: Sequelize.DataTypes.STRING,
          after: 'id',
          allowNull: false,
          unique: true,
        }, { transaction: t })
      ]);
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('users', 'last_name', { transaction: t }),
        queryInterface.removeColumn('users', 'email', { transaction: t })
      ]);
    });
  }
};
