'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class sensor extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      sensor.belongsTo(models.Door, {
        foreignKey: 'id'
      });
    }
  }
  sensor.init({
    name: DataTypes.STRING,
    door_id: {
      type: DataTypes.BIGINT,
    },
    creator_id: {
      type: DataTypes.INTEGER,
    },
  }, {
    sequelize,
    modelName: 'sensor',
  });
  return sensor;
};