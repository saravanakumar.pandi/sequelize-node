const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const route = require('./routes')

// CORS
app.use(cors());

app.use(bodyParser.json())

// Router middleware
app.use('/api',route);


app.listen(5000, () => console.log('Example app listening on port 5000!'))