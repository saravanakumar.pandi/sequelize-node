const express = require("express")
const router = express.Router();
const doorController = require('../controller/door.controller')


//To handle auth routes
router.get('/doors', doorController.getDoors);

module.exports = router;
